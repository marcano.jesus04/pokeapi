

    
    document.addEventListener('DOMContentLoaded', () => {
        const random = getRandomInt(1, 899)
        fetchdata(random)
    })
    
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (min, max)) + min;}

    const fetchdata = async (id) => {
        try {
            const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
            const data = await res.json()
            console.log(data)
        } catch{
            console.log(error)
        }
    }

    const imprimirDatos = (pokemon) => {
        console.log(pokemon)
        const flex = document.querySelector('.flex')
        const informacion = document.querySelector('#informacion-pokemon').content
        const clone = informacion.cloneNode(true)
        const fragment = document.createDocumentFragment()

        clone.querySelector('.pokemon-body-img').setAttribute('src', pokemon.sprites.front_default)

        fragment.appendChild(clone)
        flex.appendChild(fragment)
    }
